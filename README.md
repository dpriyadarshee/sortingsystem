# SortingSystem
A simple .net core web api showcasing post request from a jquery to the controller in web application and get a response back.

My first.net core web api, so use it as a learning material and not development reference.
This is just a prototype and needs improvements.

[![Build status](https://ci.appveyor.com/api/projects/status/0fwcr26fd6w6fgoc/branch/master?svg=true)](https://ci.appveyor.com/project/dp7g09/sortingsystem/branch/master)

##How to build:
From Visual Studio 2017 Developer Command Prompt v15.7.1 execute buildall.bat at root folder.

##How to run:
After building.
Open in Visual Studio 2017 v15.7.1, Press F5.