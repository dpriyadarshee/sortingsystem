﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Payment.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Payment.WebApi.Controllers
{
    public class PaymentController : Controller
    {
        IList<PaymentDto> Payments;
        public PaymentController()
        {
            Payments = new PaymentDto[]
            {
                new PaymentDto { amount =  28, currency =  "GBP", date = Convert.ToDateTime("1997/3/3"), sourceAccountNumber = "123", destinationAccountNumber = "321" },
                new PaymentDto { amount =  8, currency =  "GBP", date =  Convert.ToDateTime("2000/3/3"), sourceAccountNumber = "123", destinationAccountNumber = "321" },
                new PaymentDto { amount =  82, currency =  "GBP", date =  Convert.ToDateTime("2015/3/3"), sourceAccountNumber = "123", destinationAccountNumber = "321" },
                new PaymentDto { amount =  280, currency =  "GBP", date =  Convert.ToDateTime("1992/3/3"), sourceAccountNumber = "123", destinationAccountNumber = "321" },
                new PaymentDto { amount =  15, currency =  "GBP", date =  Convert.ToDateTime("2018/3/3"), sourceAccountNumber = "123", destinationAccountNumber = "321" }
            };
        }
    
        public IActionResult Index(IFormCollection form)
        {
            var sortby = "";
            if (string.IsNullOrEmpty(form["Sortby"]))
                sortby = "date";
            else
                sortby = form["Sortby"];

            switch (sortby.ToLowerInvariant())
            {
                case "date":
                    Payments = Payments.OrderBy(o => o.date).ToList();
                    break;
                case "amount":
                    Payments = Payments.OrderBy(o => o.amount).ToList();
                    break;
                default:
                    throw new NotImplementedException($"Sort order of {sortby} is not supported.");
            }

            return View(Payments);
        }

        [HttpGet]
        public IActionResult GetPayment(int id)
        {
            var Payment = Payments.FirstOrDefault((p) => p.amount == id);
            if (Payment == null)
            {
                return NotFound();
            }
            return Ok(Payment);
        }

        [HttpPost("{sortby}", Name = "Post")]
        public IActionResult Post([FromQuery] string sortby, [FromBody]PaymentDto[] payments)
        {
            foreach (var payment in payments)
            {
                if (payment.amount < 0)
                {
                    return BadRequest($"Payment amount must be greater than zero.");
                }
            }

            List<PaymentDto> sortedList;
            switch (sortby.ToLowerInvariant())
            {
                case "date":
                    sortedList = payments.OrderBy(o => o.date).ToList();
                    break;
                case "amount":
                    sortedList = payments.OrderBy(o => o.amount).ToList();
                    break;
                default:
                    throw new NotImplementedException($"Sort order of {sortby} is not supported.");
            }

            return Ok(sortedList);
        }
    }
}
