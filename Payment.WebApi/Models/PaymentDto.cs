﻿using System;

namespace Payment.WebApi.Models
{

    //A payment comprises:-

    //A financial value(amount)
    //A currency
    //A date
    //A source account number
    //A destination account number

    public class PaymentDto
    {
        public int amount;
        public string currency;
        public DateTime date;
        public string sourceAccountNumber;
        public string destinationAccountNumber;
    }
}