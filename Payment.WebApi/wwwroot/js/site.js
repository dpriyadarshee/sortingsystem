﻿// Write your JavaScript code.

String.prototype.compose = (function () {
    var re = /\{{(.+?)\}}/g;
    return function (o) {
        return this.replace(re, function (_, k) {
            return typeof o[k] != 'undefined' ? o[k] : '';
        });
    }
}());

$(document).ready(function () {
    var dropDown = $("#sortby");
    payments = $("#payments");

    var arrayOfPayments = [
        { "amount": 28, "currency": "GBP", "date": "1997/3/3", "sourceAccountNumber": "123", "destinationAccountNumber": "321" },
        { "amount": 8, "currency": "GBP", "date": "2000/3/3", "sourceAccountNumber": "123", "destinationAccountNumber": "321" },
        { "amount": 82, "currency": "GBP", "date": "2015/3/3", "sourceAccountNumber": "123", "destinationAccountNumber": "321" },
        { "amount": 280, "currency": "GBP", "date": "1992/3/3", "sourceAccountNumber": "123", "destinationAccountNumber": "321" },
        { "amount": 15, "currency": "GBP", "date": "2018/3/3", "sourceAccountNumber": "123", "destinationAccountNumber": "321" }
    ];

    dropDown.on("change", function () {

        $.ajax({
            type: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: "../payment?sortby=" + dropDown.val(),
            data: JSON.stringify(arrayOfPayments),
            success: function (data) {
                var tbody = payments.children("tbody");
                tbody.empty();

                var row = '<tr>' +
                    '<td>{{currency}} {{amount}}</td>' +
                    '<td>{{date}}</td>' +
                    '<td>{{sourceAccountNumber}}</td>' +
                    '<td>{{destinationAccountNumber}}</td>' +
                    '</tr>';
                $.each(data, function (index, value) {
                    tbody.append(row.compose(value));
                });
            },
            dataType: "json"
        });
    });

});